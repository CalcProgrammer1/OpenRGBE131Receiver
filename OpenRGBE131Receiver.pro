#-----------------------------------------------------------------------#
# OpenRGB E1.31 Receiver QMake Project                                  #
#                                                                       #
#   Adam Honse (CalcProgrammer1)                        6/15/2020       #
#-----------------------------------------------------------------------#

#-----------------------------------------------------------------------#
# Application Configuration                                             #
#-----------------------------------------------------------------------#
VERSION     = 0.01
TARGET      = OpenRGBE131Receiver
TEMPLATE    = app
CONFIG     += console
CONFIG     -= app_bundle
CONFIG     -= qt
CONFIG     += no_batch

#-----------------------------------------------------------------------#
# OpenRGB E1.31 Receiver Common                                         #
#-----------------------------------------------------------------------#
INCLUDEPATH +=                                                          \
    dependencies/libe131/src/                                           \

HEADERS +=                                                              \
    dependencies/libe131/src/e131.h                                     \

SOURCES +=                                                              \
    dependencies/libe131/src/e131.c                                     \
    main.cpp                                                            \

#-----------------------------------------------------------------------#
# OpenRGB SDK                                                           #
#-----------------------------------------------------------------------#
INCLUDEPATH +=                                                          \
    OpenRGB/                                                            \
    OpenRGB/net_port/                                                   \
    OpenRGB/RGBController/                                              \

HEADERS +=                                                              \
    OpenRGB/NetworkClient.h                                             \
    OpenRGB/NetworkProtocol.h                                           \
    OpenRGB/net_port/net_port.h                                         \
    OpenRGB/RGBController/RGBController.h                               \
    OpenRGB/RGBController/RGBController_Network.h                       \

SOURCES +=                                                              \
    OpenRGB/NetworkClient.cpp                                           \
    OpenRGB/net_port/net_port.cpp                                       \
    OpenRGB/RGBController/RGBController.cpp                             \
    OpenRGB/RGBController/RGBController_Network.cpp                     \

#-----------------------------------------------------------------------#
# Windows-specific Configuration                                        #
#-----------------------------------------------------------------------#
win32:contains(QMAKE_TARGET.arch, x86_64) {
    LIBS +=                                                             \
        -lws2_32                                                        \
}

win32:contains(QMAKE_TARGET.arch, x86) {
    LIBS +=                                                             \
        -lws2_32                                                        \
}

win32:DEFINES -=                                                        \
    UNICODE

win32:DEFINES +=                                                        \
    _MBCS                                                               \
    WIN32                                                               \
    _CRT_SECURE_NO_WARNINGS                                             \
    _WINSOCK_DEPRECATED_NO_WARNINGS                                     \
    WIN32_LEAN_AND_MEAN

#-----------------------------------------------------------------------#
# Linux-specific Configuration                                          #
#-----------------------------------------------------------------------#
unix:!macx {
    LIBS +=                                                             \
    -lpthread                                                           \
}