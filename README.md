# OpenRGB E1.31 Receiver

This project implements an E1.31 multicast receiver that allows E1.31 lighting control applications to control OpenRGB devices.

At the moment, there are no configurable parameters.  When you open this program it will try to connect to the local OpenRGB server with the default port (127.0.0.1:1337) and request a list of devices.  It then opens one E1.31 universe per device.  Color data on these universes is RGB order, 3 bytes per color, beginning at address 1.  Due to the 512 slot limitation of an E1.31 universe, devices with more than 170 LEDs are not currently supported.  The first 170 LEDs will work and the rest will be uncontrollable, remaining at their previous state.  Zones and mappings must be configured in your E1.31 lighting control software.

This project has been tested using Vixen Lights 3 (http://www.vixenlights.com/).